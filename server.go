package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	jsoniter "github.com/json-iterator/go"
	"gitlab.com/feiranwang/craft"
)

var (
	logger = log.New(os.Stderr, "", log.LstdFlags)
)

// Clock implements craft's Clock interface
type Clock struct{}

// GetClockUncertainty here is a dummy implementation
func (c *Clock) GetClockUncertainty() int {
	return 1
}

// LocalFSM implements fsm, which is a dummy implementation
type LocalFSM struct{}

// Apply applies a log
func (f *LocalFSM) Apply(*craft.Log) interface{} {
	return nil
}

// Snapshot takes a snapshot
func (f *LocalFSM) Snapshot() (craft.FSMSnapshot, error) {
	return &fsmSnapshot{}, nil
}

// Restore restores from snapshot
func (f *LocalFSM) Restore(io.ReadCloser) error {
	return nil
}

// KvFSM implements CFSM
type KvFSM struct {
	store map[string]string
}

// Apply applies the merger entry
func (f *KvFSM) Apply(entry *craft.MergerEntry) interface{} {
	var c command
	var resp interface{}
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	err := json.Unmarshal(entry.Log.Data, &c)
	if err != nil {
		logger.Printf("[ERROR] failed to unmarshal command: %s\n", err.Error())
	}
	switch c.Op {
	case "set":
		f.store[c.Key] = c.Value
	case "get":
		v, ok := f.store[c.Key]
		if ok {
			resp = v
		}
	default:
		logger.Printf("[ERROR] unrecognized command op: %s\n", c.Op)
	}

	if entry.Future != nil {
		entry.Future.SetResponse(resp)
		entry.Future.Complete()
	}

	return nil
}

type fsmSnapshot struct{}

func (f *fsmSnapshot) Persist(sink craft.SnapshotSink) error {
	return nil
}

func (f *fsmSnapshot) Release() {}

type command struct {
	Op    string
	Key   string
	Value string
}

// Service provides a key-value store service
type Service struct {
	raftInstances []*craft.Raft
	clusterConfig ClusterConfig
	serverID      int
	leader        *craft.Raft
}

// runRaftServer starts a new Raft server
func runRaftServer(clusterConfig ClusterConfig, groupID int, serverID int) (*craft.Raft, error) {

	serverConfig := clusterConfig.GetServerConfig(groupID, serverID)

	config := craft.DefaultConfig()
	config.LocalID = craft.ServerID(strconv.Itoa(serverID))

	fullAddress := getFullAddress(serverConfig.Addr, serverConfig.RaftPort)
	transport, err := craft.NewTCPTransport(fullAddress, nil, 3, 10*time.Second, os.Stderr)
	if err != nil {
		return nil, err
	}

	logStore := craft.NewInmemStore()
	stableStore := craft.NewInmemStore()
	snapshotStore := craft.NewInmemSnapshotStore()

	KvFSM := &LocalFSM{}

	raftInstance, err := craft.NewRaft(config, KvFSM, logStore, stableStore, snapshotStore, transport)
	if err != nil {
		return nil, err
	}

	return raftInstance, nil
}

func (service *Service) handler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/bootstrap" {
		service.handleBootstrap(w, r)
		logger.Println("[INFO] bootstrap")
	} else if strings.HasPrefix(r.URL.Path, "/request") {
		service.handleRequest(w, r)
	} else {
		logger.Println("[INFO] Unhandled request!")
	}
}

// handleBootstrap bootstraps the cluster
func (service *Service) handleBootstrap(w http.ResponseWriter, r *http.Request) {
	nGroups := len(service.clusterConfig.Servers)
	for i := 0; i < nGroups; i++ {
		serverConfigs := service.clusterConfig.Servers[i]
		servers := make([]craft.Server, len(serverConfigs))
		for j := 0; j < len(serverConfigs); j++ {
			config := serverConfigs[j]
			servers[j] = craft.Server{
				ID:       craft.ServerID(strconv.Itoa(config.ID)),
				Address:  craft.ServerAddress(getFullAddress(config.Addr, config.RaftPort)),
				Priority: config.Priority,
			}
		}
		config := craft.Configuration{
			Servers: servers,
		}
		service.raftInstances[i].BootstrapCluster(config)
	}
}

func (service *Service) handleRequest(w http.ResponseWriter, r *http.Request) error {
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) != 2 {
		w.WriteHeader(http.StatusBadRequest)
		return errors.New("Invalid request format")
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return err
	}

	// find a leader
	if service.leader == nil {
		for _, r := range service.raftInstances {
			if r.IsLeader() {
				service.leader = r
			}
		}
		if service.leader == nil {
			w.WriteHeader(http.StatusInternalServerError)
			return errors.New("No leader found on this server")
		}
	}

	f := service.leader.Apply(body, 10*time.Second)
	if err := f.Error(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return err
	}
	var c command
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	// no error checking because it it checked in Apply above
	json.Unmarshal(body, &c)

	// wait for the log to be executed
	f.Wait()

	// get the response from the fsm
	resp := f.Response()

	switch c.Op {
	case "set":
		io.WriteString(w, fmt.Sprintf("Successfully set key %v to value %v\n", c.Key, c.Value))
	case "get":
		if resp != nil {
			io.WriteString(w, fmt.Sprintf("Get key: %v, value: %v\n", c.Key, resp))
		} else {
			io.WriteString(w, fmt.Sprintf("Key %v does not exist\n", c.Key))
		}
	}
	return nil
}

// NewService initializes a new replicated key-value store service
func NewService(serverID int) *Service {

	clusterConfig, err := LoadClusterConfig()
	if err != nil {
		panic(err.Error())
	}

	nGroups := len(clusterConfig.Servers)
	clock := &Clock{}

	raftInstances := make([]*craft.Raft, nGroups)
	for i := 0; i < nGroups; i++ {
		raftInstance, err := runRaftServer(clusterConfig, i, serverID)
		if err != nil {
			panic(err.Error())
		}
		raftInstances[i] = raftInstance
	}

	store := make(map[string]string)
	kvfsm := &KvFSM{
		store: store,
	}
	merger := craft.NewMerger(nGroups, kvfsm)

	for i := 0; i < nGroups; i++ {
		err = raftInstances[i].ConfigureGroups(i, raftInstances, merger, clock)
		if err != nil {
			panic(err.Error())
		}
	}

	service := &Service{
		raftInstances: raftInstances,
		clusterConfig: clusterConfig,
		serverID:      serverID,
		leader:        nil,
	}

	http.HandleFunc("/", service.handler)
	server := clusterConfig.GetServerConfig(0, serverID)
	httpPort := server.HttpPort
	err = http.ListenAndServe(fmt.Sprintf(":%d", httpPort), nil)
	logger.Println(err)

	return service
}

func main() {
	serverIDPtr := flag.Int("id", -1, "server id")
	flag.Parse()
	if *serverIDPtr < 0 {
		logger.Println("[ERROR] Invalid server id")
		return
	}
	NewService(*serverIDPtr)

	terminate := make(chan os.Signal, 1)
	signal.Notify(terminate, os.Interrupt)
	<-terminate
}

func getFullAddress(addr string, port int) string {
	return fmt.Sprintf("%s:%d", addr, port)
}
