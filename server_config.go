package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// ServerConfig for raft
type ServerConfig struct {
	ID       int    `json:"id"`
	Addr     string `json:"addr"`
	Priority int    `json:"priority"`
	RaftPort int    `json:"raftPort"`
	HttpPort int    `json:"httpPort"`
}

// ClusterConfig for raft
type ClusterConfig struct {
	Servers [][]ServerConfig `json:"servers"`
}

// GetServerConfig
func (config *ClusterConfig) GetServerConfig(groupID int, serverID int) ServerConfig {
	return config.Servers[groupID][serverID]
}

// LoadClusterConfig
func LoadClusterConfig() (ClusterConfig, error) {
	file, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println("Loading server config error")
		return ClusterConfig{}, err
	}
	var config ClusterConfig
	if err = json.Unmarshal(file, &config); err != nil {
		fmt.Println(err)
	}
	return config, nil
}

// NumberOfServers returns number of servers
func (config *ClusterConfig) NumberOfServers() int {
	if len(config.Servers) == 0 {
		return 0
	}
	return len(config.Servers[0])
}
