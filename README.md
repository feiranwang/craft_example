craft example
====

This is an example use of craft. CRaft is a consensus protocol, whose purpose is to ensure
that a set of nodes agree on the state of some state machine under node failures or network partitions.
The example is a in-memory key-value store. It supports `set` and `get` operations.
Every operation is first logged and replicated by craft before it is executed.

## Running the example

### Building the example
Building the example requires Go 1.9 or later. Install [Go](https://golang.org/) before proceeding.

Download the example like so:
```bash
go get gitlab.com/feiranwang/craft_example
```

Go to the downloaded source folder, which should be at `$GOPATH/src/gitlab.com/feiranwang/craft_example`.
`$GOPATH` is the Go workspace directory. If it is not set, the default workspace directory is `$HOME/go`.
Compile the server:

```bash
go build
```

### Configuring the cluster

Use `config.json` to configure the cluster. The `servers` is a list of Raft groups.
Each group will contain a number of servers, or Raft instances. The typical numbers are 3, 5, and 7.
The source folder includes an example configuration shown below.
It is a two-group and three-server configuration, and can tolerate one failure.
<!-- Each Raft instance has the following configurations `id`, which is the server id, `raftPort`, which is the port Raft uses to communicate internally, `` -->

```json
{
    "servers": [
        [
            {
                "raftPort": 9000, 
                "httpPort": 10080, 
                "priority": 5, 
                "id": 0, 
                "addr": "127.0.0.1"
            }, 
            {
                "raftPort": 9001, 
                "httpPort": 10081, 
                "priority": 1, 
                "id": 1, 
                "addr": "127.0.0.1"
            }, 
            {
                "raftPort": 9002, 
                "httpPort": 10082, 
                "priority": 1, 
                "id": 2, 
                "addr": "127.0.0.1"
            }
        ], 
        [
            {
                "raftPort": 9010, 
                "httpPort": 10080, 
                "priority": 1, 
                "id": 0, 
                "addr": "127.0.0.1"
            }, 
            {
                "raftPort": 9011, 
                "httpPort": 10081, 
                "priority": 5, 
                "id": 1, 
                "addr": "127.0.0.1"
            }, 
            {
                "raftPort": 9012, 
                "httpPort": 10082, 
                "priority": 1, 
                "id": 2, 
                "addr": "127.0.0.1"
            }
        ]
    ]
}
```

### Bringing up the cluster

Run each server on different shell sessions like so:
```bash
./craft_example -id=0
./craft_example -id=1
./craft_example -id=2
```

Now bootstrap the cluster:
```bash
curl -XGET localhost:10080/bootstrap
```

Wait a few seconds until the cluster stablizes.

### Testing the example
After the bootstrap finishes, the server can serve `set` or `get` requests. You can set a key by
```bash
curl -XPOST localhost:10080/request -d '{"Op": "set", "Key": "a", "Value": "1"}'
```

You will see the following information if the operation is successful
```
Successfully set key a to value 1
```

You can read the value for a key by
```bash
curl -XPOST localhost:10080/request -d '{"Op": "get", "Key": "a"}'
```

It will return
```
Get key: a, value: 1
```


### Tolerating failure
Kill the leader process and watch one of the other nodes be elected leader. The keys are still available for query on the other nodes, and you can set keys on the new leader. Furthermore, when the first node is restarted, it will rejoin the cluster and learn about any updates that occurred while it was down.

A 3-node cluster can tolerate the failure of a single node, but a 5-node cluster can tolerate the failure of two nodes. But 5-node clusters require that the leader contact a larger number of nodes before any change e.g. setting a key's value, can be considered committed.

### Multi-node cluster deployment and clock synchronization
Deploying this example onto a set of nodes follow similar procedures.
As craft relies on synchronized clocks, the first step of the deployment is to run clock synchronization to synchronize the clocks in the cluster.
To get the best performance, the precision of clocks (difference between any two clocks in the network)
should be less than the one-way network delay between two nodes.

craft also needs to know the clock uncertainty, which is the max possible clock offsets
between any two clocks in the network, at any given time.
The developers should implement the `Clock` interface.
